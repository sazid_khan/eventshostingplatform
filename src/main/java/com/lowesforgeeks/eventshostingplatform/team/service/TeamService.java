package com.lowesforgeeks.eventshostingplatform.team.service;

import com.lowesforgeeks.eventshostingplatform.member.entity.Member;
import com.lowesforgeeks.eventshostingplatform.member.service.MemberService;
import com.lowesforgeeks.eventshostingplatform.team.entity.Team;
import com.lowesforgeeks.eventshostingplatform.team.repository.TeamRepository;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private MemberService memberService;



    //Create Team
    public Object createTeam(String teamName, String teamAdminEmail) throws Exception {
        Member teamAdmin = memberService.getByEmail(teamAdminEmail);
        if (teamAdmin == null) {
            throw new Exception("No "+teamAdminEmail+" found in the database");
        } else {
            return teamRepository.save(new Team(teamName, teamAdmin));
        }
    }

    // Retrieve team by teamName(team name is unique)
    public Object getByTeamName(String teamName) throws Exception{
        Team team = teamRepository.findByTeamName(teamName);
        if(team == null){
            throw new Exception("teamName: "+teamName+" not found in the database");
        }
        else {
            return team;
        }
    }

    public Object getByTeamAdmin(String email) throws Exception{
        Team team = teamRepository.findByTeamAdminEmail(email);
        if(team == null){
            throw new Exception("Cannot find team with teamAdminEmail: "+email);
        }
        else {
            return team;
        }

    }

    //Update an existing team
    public Object updateTeamName(String teamAdminEmail, String newTeamName) throws Exception {
        Team team = (Team) getByTeamAdmin(teamAdminEmail);

        team.setTeamName(newTeamName);
        return team;

    }

    public Object updateTeamAdmin(String teamName, String newTeamAdminEmail) throws Exception {
        Team team = (Team) getByTeamName(teamName);
        team.setTeamAdminEmail(newTeamAdminEmail);
        return team;
    }

    public Object removeExistingMember(String teamName,List<String> deleteMembersEmailId) throws Exception {
        Team team = (Team) getByTeamName(teamName);
        List<Member> teamMembers = team.getMembers();

        for(String deleteMemberEmailId : deleteMembersEmailId) {
            teamMembers.remove(memberService.getByEmail(deleteMemberEmailId));
        }
        team.setMembers(teamMembers);
        return team;
    }

    public Object addMember(String teamName, List<String> newMembersEmailId) throws Exception {
        Team team = (Team) getByTeamName(teamName);

        List<Member> teamMembers = team.getMembers();

        for(String newMemberEmailId : newMembersEmailId){
            teamMembers.add(memberService.getByEmail(newMemberEmailId));
        }
        team.setMembers(teamMembers);
        return team;
    }
}
