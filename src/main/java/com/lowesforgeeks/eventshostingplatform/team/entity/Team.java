package com.lowesforgeeks.eventshostingplatform.team.entity;

import com.lowesforgeeks.eventshostingplatform.member.entity.Member;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Document(collection = "Teams")
public class Team {

    @Id
    private String id;

    @Indexed(unique = true)
    @NotBlank(message = "teamName cannot be blank")
    public String teamName;
    public List<Member> members;

    @NotBlank(message = "teamAdminEmail cannot be blank")
    public String teamAdminEmail;

    public Member teamAdmin;

    public Team(String teamName, Member teamAdmin) {
        this.teamName = teamName;
        this.teamAdmin = teamAdmin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public String getTeamAdminEmail() {
        return teamAdminEmail;
    }

    public void setTeamAdminEmail(String teamAdminEmail) {
        this.teamAdminEmail = teamAdminEmail;
    }
}
