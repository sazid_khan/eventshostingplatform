package com.lowesforgeeks.eventshostingplatform.team.controller;

import com.lowesforgeeks.eventshostingplatform.member.entity.Member;
import com.lowesforgeeks.eventshostingplatform.team.entity.Team;
import com.lowesforgeeks.eventshostingplatform.team.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/lowesforgeeks/team")
@CrossOrigin
public class TeamController {

    @Autowired
    TeamService teamService;


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Object createTeam(@Valid @RequestBody Team request,BindingResult result) throws Exception {
        if(result.hasErrors()){
                Map<String,String> errorMap = new HashMap<>();
                for(FieldError error:result.getFieldErrors()){
                    errorMap.put(error.getField(),error.getDefaultMessage());
                }
                return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
            }
            Team team = (Team) teamService.createTeam(request.teamName, request.teamAdminEmail);
            return new ResponseEntity<Team>(team, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public Object viewTeam(
            @RequestParam(value = "teamName", required = false, defaultValue = "") String teamName,
            @RequestParam(value = "teamAdminEmail", required = false, defaultValue = "") String teamAdminEmail) throws Exception{
        if(teamAdminEmail.isEmpty() && teamName.isEmpty()) return "teamAdminEmail and teamName cannot be empty at the same time";
        if(teamName.isEmpty()) return teamService.getByTeamAdmin(teamAdminEmail);
        else return teamService.getByTeamName(teamName);
    }

    @RequestMapping(value = "/update/team-admin", method = RequestMethod.PUT)
    public Object updateTeamAdmin(@RequestParam String teamName, @RequestParam String teamAdminEmail) throws Exception{
        return teamService.updateTeamAdmin(teamName,teamAdminEmail);
    }

    @RequestMapping(value = "/update/team-name", method = RequestMethod.PUT)
    public Object updateTeamName(@RequestParam String teamAdminEmail, @RequestParam String teamName) throws Exception{
        return teamService.updateTeamName(teamAdminEmail,teamName);
    }

    @RequestMapping(value = "/update/add-members", method = RequestMethod.PUT)
    public Object addTeamMembers(@RequestParam String teamName,@RequestBody List<String> members) throws Exception{
        return teamService.addMember(teamName,members);
    }

    @RequestMapping(value = "/update/remove-member", method = RequestMethod.PUT)
    public Object removeTeamMembers(@RequestParam String teamName, @RequestBody List<String> members) throws Exception{
        return teamService.removeExistingMember(teamName,members);
    }
}
