package com.lowesforgeeks.eventshostingplatform.team.repository;

import com.lowesforgeeks.eventshostingplatform.team.entity.Team;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public interface TeamRepository extends MongoRepository<Team, String> {
    public Team findByTeamName(String teamName);
    public Team findByTeamAdminEmail(String email);
}
