package com.lowesforgeeks.eventshostingplatform.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum MemberRoleEnum {
    ORGANISATION_ADMIN("ORGANISATION ADMIN"),
    TEAM_ADMIN("TEAM ADMIN"),
    NORMAL_MEMBER("NORMAL MEMBER");

    private final String name;
}
