package com.lowesforgeeks.eventshostingplatform.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum  EventTypeEnum {
    ORG("organisation"),
    TEAM("team"),
    PRIVATE("private");

    private final String name;
}
