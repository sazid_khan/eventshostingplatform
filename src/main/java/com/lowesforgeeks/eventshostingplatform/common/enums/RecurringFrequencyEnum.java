package com.lowesforgeeks.eventshostingplatform.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum RecurringFrequencyEnum {
    D("day"),
    W("week"),
    M("month"),
    Y("year");

    private final String name;
}
