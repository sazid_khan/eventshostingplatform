package com.lowesforgeeks.eventshostingplatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class },scanBasePackages = {"com.lowesforgeeks.eventshostingplatform.member","com.lowesforgeeks.eventshostingplatform.team"})

public class EventshostingplatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventshostingplatformApplication.class, args);
	}

}
