package com.lowesforgeeks.eventshostingplatform.event.entity;

import com.lowesforgeeks.eventshostingplatform.common.enums.EventTypeEnum;
import com.lowesforgeeks.eventshostingplatform.common.enums.RecurringFrequencyEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.lang.reflect.Member;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Document(collection = "Events")
public class Event {
    @Id
    private String id;

    @NotBlank(message = "Event type cannot be blank")
    private EventTypeEnum eventType;

    @Indexed(unique = true)
    @NotBlank(message = "Event name cannot be blank")
    private String name;

    private String description;

    @NotBlank(message = "Event admin cannot be left empty")
    private Member createdBy;
    private String location;

    @NotBlank(message = "Start date cannot be left empty")
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    private Long numberOfLikes;
    private Long numberOfWatchers;
    private Long numberOfViews;
    private Long numberOfParticipants;
    private Boolean recurring;
    private RecurringFrequencyEnum recurringFrequency;
    public List<Member> participants;

    public Event(EventTypeEnum eventType, @NotBlank(message = "Event name cannot be blank") String name, String description, Member createdBy, String location, LocalDateTime startDate, LocalDateTime endDate, Long numberOfLikes, Long numberOfWatchers, Long numberOfViews, Long numberOfParticipants, Boolean recurring, RecurringFrequencyEnum recurringFrequency) {
        this.eventType = eventType;
        this.name = name;
        this.description = description;
        this.createdBy = createdBy;
        this.location = location;
        this.startDate = startDate;
        this.endDate = endDate;
        this.numberOfLikes = numberOfLikes;
        this.numberOfWatchers = numberOfWatchers;
        this.numberOfViews = numberOfViews;
        this.numberOfParticipants = numberOfParticipants;
        this.recurring = recurring;
        this.recurringFrequency = recurringFrequency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Member getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Member createdBy) {
        this.createdBy = createdBy;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Long getNumberOfLikes() {
        return numberOfLikes;
    }

    public void setNumberOfLikes(Long numberOfLikes) {
        this.numberOfLikes = numberOfLikes;
    }

    public Long getNumberOfWatchers() {
        return numberOfWatchers;
    }

    public void setNumberOfWatchers(Long numberOfWatchers) {
        this.numberOfWatchers = numberOfWatchers;
    }

    public Long getNumberOfViews() {
        return numberOfViews;
    }

    public void setNumberOfViews(Long numberOfViews) {
        this.numberOfViews = numberOfViews;
    }

    public Long getNumberOfParticipants() {
        return numberOfParticipants;
    }

    public void setNumberOfParticipants(Long numberOfParticipants) {
        this.numberOfParticipants = numberOfParticipants;
    }

    public Boolean getRecurring() {
        return recurring;
    }

    public void setRecurring(Boolean recurring) {
        this.recurring = recurring;
    }

    public RecurringFrequencyEnum getRecurringFrequency() {
        return recurringFrequency;
    }

    public void setRecurringFrequency(RecurringFrequencyEnum recurringFrequency) {
        this.recurringFrequency = recurringFrequency;
        if(recurringFrequency != null)
            setStartDate(LocalDateTime.now());
//        if(recurringFrequency != null){
//            if(getRecurring().equals(RecurringFrequencyEnum.D)){
//                setStartDate(LocalDateTime.now());
//                setEndDate(LocalDateTime.now());
//            }
//            if(getRecurring().equals(RecurringFrequencyEnum.W)){
//                setStartDate(LocalDateTime.now());
//                setEndDate(LocalDateTime.now().plusWeeks(1));
//            }
//            if(getRecurring().equals(RecurringFrequencyEnum.M)){
//                setStartDate(LocalDateTime.now());
//                setEndDate(LocalDateTime.now().plusMonths(1));
//            }
//            if(getRecurring().equals(RecurringFrequencyEnum.Y)){
//                setStartDate(LocalDateTime.now());
//                setEndDate(LocalDateTime.now().plusYears(1));
//            }
//        }
    }
}
