package com.lowesforgeeks.eventshostingplatform.event.repository;

import com.lowesforgeeks.eventshostingplatform.event.entity.Event;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends MongoRepository<Event, String> {
    public Event findByName(String name);

}
