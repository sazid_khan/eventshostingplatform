package com.lowesforgeeks.eventshostingplatform.event.service;

import com.lowesforgeeks.eventshostingplatform.common.enums.EventTypeEnum;
import com.lowesforgeeks.eventshostingplatform.common.enums.MemberRoleEnum;
import com.lowesforgeeks.eventshostingplatform.common.enums.RecurringFrequencyEnum;
import com.lowesforgeeks.eventshostingplatform.event.entity.Event;
import com.lowesforgeeks.eventshostingplatform.event.repository.EventRepository;
import com.lowesforgeeks.eventshostingplatform.member.entity.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class EventService {
    @Autowired
    private EventRepository eventRepository;

    public Object createEvent(EventTypeEnum eventType, String name, String desc, Member createdBy, String location, LocalDateTime startDate,
                              LocalDateTime endDate, Boolean recurring, RecurringFrequencyEnum recurringFrequency) throws Exception {

        //validate Event
        validateEvent(name, createdBy, eventType, startDate);
        return eventRepository.save(new Event(eventType, name, desc, (java.lang.reflect.Member) createdBy, location, startDate, endDate,
                0L, 0L,0L,0L,recurring, recurringFrequency));

    }

    public Object validateEvent(String name, Member createdBy, EventTypeEnum eventType, LocalDateTime startDate) throws Exception{
        Event checkEvent = eventRepository.findByName(name);
        if(checkEvent != null){
            throw new Exception("Team with team name '"+name+"' already exist");
        }

        if(createdBy.role.equals(MemberRoleEnum.TEAM_ADMIN) && eventType.equals(EventTypeEnum.ORG)){
            throw new Exception("Event is of higher order");
        }

        if(eventType.equals(EventTypeEnum.ORG)){
            LocalDateTime today = LocalDateTime.now();
            if(startDate.plusMonths(2).isAfter(today)) {
                throw new Exception("Its late to host this event");
            }
        }

        if(eventType.equals((EventTypeEnum.TEAM))){
            LocalDateTime today = LocalDateTime.now();
            if(startDate.plusWeeks(1).isAfter(today)){
                throw new Exception("Its late to host this event");
            }
        }

        if(eventType.equals(EventTypeEnum.PRIVATE)){
            LocalDateTime today = LocalDateTime.now();
            if(startDate.plusDays(2).isAfter(today)){
                throw new Exception("Its late to host this event");
            }
        }
        return "";
    }
}
