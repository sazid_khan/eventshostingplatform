package com.lowesforgeeks.eventshostingplatform.member.controller;

import com.lowesforgeeks.eventshostingplatform.member.dto.MemberDto;
import com.lowesforgeeks.eventshostingplatform.member.entity.Member;
import com.lowesforgeeks.eventshostingplatform.member.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/lowesforgeeks/member")
@CrossOrigin
public class MemberController {

    @Autowired
    private MemberService memberService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> create(@Valid  @RequestBody Member memberDetails, BindingResult result){
        if(result.hasErrors()){
            Map<String,String> errorMap = new HashMap<>();
            for(FieldError error:result.getFieldErrors()){
                errorMap.put(error.getField(),error.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }
        Member member = memberService.createMember(memberDetails.firstName, memberDetails.lastName, memberDetails.email);
        return new ResponseEntity<Member>(member,HttpStatus.CREATED);
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public Member getMember(@RequestParam String email){
        Member member = memberService.getByEmail(email);

        return member;
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public Member updateMember(@RequestParam String firstName, @RequestParam String lastName , @RequestParam String email){
        Member member = memberService.updateMember(firstName, lastName, email);

        return member;
    }

    @RequestMapping(value = "/delete",method = RequestMethod.DELETE)
    public String deleteMember(@RequestParam String email,@RequestParam Id id) {
        memberService.deleteMember(email);
        return "member with email"+email+"deleted";
    }

    @RequestMapping(value = "/delete-all", method = RequestMethod.DELETE)
    public String deleteAllMember(){
        memberService.deleteAllMembers();
        return "All record has been deleted";
    }
}
