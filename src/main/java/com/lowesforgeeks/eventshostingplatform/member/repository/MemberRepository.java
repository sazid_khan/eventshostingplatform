package com.lowesforgeeks.eventshostingplatform.member.repository;

import com.lowesforgeeks.eventshostingplatform.member.entity.Member;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
interface MemberRepository extends MongoRepository<Member,String> {
    public Member findById(Id id);
    public List<Member> findByFirstName(String firstName);
    public List<Member> findByLastName(String lastName);
    public Member findByEmail(String email);
}
