package com.lowesforgeeks.eventshostingplatform.member.entity;

import com.lowesforgeeks.eventshostingplatform.common.enums.MemberRoleEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;


@Document(collection = "Members")
public class Member {


    @Id
    private String id;

    @NotBlank(message = "firstname cannot be blank")
    public String firstName;
    public String lastName;

    @NotBlank(message = "Email cannot be blank")
    public String email;

    public MemberRoleEnum role;

    public Member(String firstName, String lastName, String email){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
