package com.lowesforgeeks.eventshostingplatform.member.service;

import com.lowesforgeeks.eventshostingplatform.member.entity.Member;
import com.lowesforgeeks.eventshostingplatform.member.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Service;

@Service
public class MemberService {

    @Autowired
    private MemberRepository memberRepository;

    //create member
    public Member createMember(String firstName, String lastName, String email){
        return memberRepository.save(new Member(firstName, lastName, email));
    }

    //Retrieve Member details using email or id
    public Member getByEmail(String email){
        Member member = memberRepository.findByEmail(email);
        return member;
    }

    //Update Member
    public Member updateMember(String firstName, String lastName, String email){
        Member member = getByEmail(email);
        member.setFirstName(firstName);
        member.setLastName(lastName);
        member.setEmail(email);

        return memberRepository.save(member);
    }

    //delete member
    public void deleteMember(String email){
        Member member = getByEmail(email);
        memberRepository.delete(member);
    }

    //Delete all member
    public void deleteAllMembers(){
        memberRepository.deleteAll();
    }

}
